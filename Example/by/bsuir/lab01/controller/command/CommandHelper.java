package by.bsuir.lab01.controller.command;

import java.util.HashMap;
import java.util.Map;

import by.bsuir.lab01.controller.command.impl.book.AddNewBookCommand;
import by.bsuir.lab01.controller.command.impl.user.LoginCommand;

public final class CommandHelper {
	private Map<CommandName, Command> commands = new HashMap<CommandName, Command>();
	
	public CommandHelper(){
		commands.put(CommandName.ADD_NEW_BOOK, new AddNewBookCommand());
		commands.put(CommandName.LOGIN_USER, new LoginCommand());
	}	
	
	public Command getCommand(String commandName){
		CommandName command = CommandName.valueOf(commandName);
		return commands.get(command);		
	}
}
