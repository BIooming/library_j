### What is this repository for? ###

* This is an example of implementation DAO in command processor pattern within layered architecture. 
* Application is simple console realisation of home library controller. Has two roles - Admin and User.
* User can only review book repository
* Admin can edit book repository and add new users to system
* Version 0.4

### Who do I talk to? ###

* Repo owner - BIooming (4.work.vifi@gmail.com)