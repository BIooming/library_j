package by.bsuir.lab01.entity;

import by.bsuir.lab01.bean.book.NewBookRequest;

public class Book {
    private String title;
    private String author;
    private BookType type;

    public Book(NewBookRequest request) {
        this.author = request.getAuthor();
        this.title = request.getTitle();
        this.type = request.getType();
    }

    public Book() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public BookType getType() {
        return type;
    }

    public void setType(BookType type) {
        this.type = type;
    }
}
