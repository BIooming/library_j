package by.bsuir.lab01.entity;

/**
 * Created by blooming on 25.9.15.
 */
public class UserCreationException extends Exception {
    private static final long serialVersionUID = 1L;

    public UserCreationException(String message) {
        super(message);
    }

    public UserCreationException(String message, Exception ex) {
        super(message, ex);
    }
}
