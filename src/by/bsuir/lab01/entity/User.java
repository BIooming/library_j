package by.bsuir.lab01.entity;

import by.bsuir.lab01.bean.encription.Encryptor;
import by.bsuir.lab01.bean.encription.EncryptorException;
import by.bsuir.lab01.bean.user.NewUserRequest;

/**
 * Created by blooming on 25.9.15.
 */
public class User {
    private String userName;
    private String email;
    private String password;
    private UserType userType;

    public User() {

    }

    public User(NewUserRequest request) throws UserCreationException {
        try {
            this.userName = request.getUserName();
            this.email = request.getEmail();
            this.userType = request.getUserType();
            this.password = Encryptor.encrypt(request.getPassword());
        } catch (Exception e) {
            throw new UserCreationException(e.getMessage());
        }
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password.toString();
    }

    public void setPassword(String password) throws UserCreationException {
        try {
            this.password = Encryptor.encrypt(password);
        } catch (EncryptorException ex) {
            throw new UserCreationException(ex.getMessage());
        }
    }

    public void setHashedPassword(String password) {
        this.password = password;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }
}
