package by.bsuir.lab01.entity;

/**
 * Created by blooming on 25.9.15.
 */
public enum UserType {
    GUEST, ADMIN
}
