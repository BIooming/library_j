package by.bsuir.lab01.start;

import by.bsuir.lab01.view.View;

public class Main {
    private static View view;

    public static void main(String[] args) {
        view = new View();
        view.menu();
    }
}
