package by.bsuir.lab01.controller;

import by.bsuir.lab01.bean.Request;
import by.bsuir.lab01.bean.Response;
import by.bsuir.lab01.controller.command.Command;
import by.bsuir.lab01.controller.command.CommandException;
import by.bsuir.lab01.controller.command.CommandHelper;

public class UserController {
    private CommandHelper commandList = new CommandHelper();

    public Response executeRequest(Request request) {
        Response response = null;

        try {
            String commandName = request.getCommandName();
            Command command = commandList.getCommand(commandName);
            response = command.execute(request);
        } catch (CommandException e) {
            response = new Response();
            response.setErrorMessage("User Controller Exception"
                    + e.getMessage());
        }

        return response;
    }
}
