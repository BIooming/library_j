package by.bsuir.lab01.controller.command.impl.book;

import by.bsuir.lab01.bean.Request;
import by.bsuir.lab01.bean.Response;
import by.bsuir.lab01.bean.book.DeleteBookRequest;
import by.bsuir.lab01.bean.book.DeleteBookResponse;
import by.bsuir.lab01.controller.command.Command;
import by.bsuir.lab01.controller.command.CommandException;
import by.bsuir.lab01.controller.command.CommandName;
import by.bsuir.lab01.entity.Book;
import by.bsuir.lab01.service.ModificationRepositoryService;
import by.bsuir.lab01.service.ServiceException;

/**
 * Created by blooming on 5.10.15.
 */
public class DeleteBookCommand implements Command {

    @Override
    public Response execute(Request request) throws CommandException {

        if (!validationParameters(request)) {
            throw new CommandException("Request parameters are invalid");
        }

        DeleteBookRequest deleteBookRequest = (DeleteBookRequest) request;
        Book book = new Book();
        book.setAuthor(deleteBookRequest.getAuthor());
        book.setTitle(deleteBookRequest.getTitle());
        book.setType(deleteBookRequest.getType());

        boolean result = false;
        try {
            result = ModificationRepositoryService
                    .deleteBookService(book);
        } catch (ServiceException e) {
            throw new CommandException("Delete book service exception:", e);
        }

        DeleteBookResponse response = new DeleteBookResponse();
        if (result) {
            response.setResultMessage("Book deleted.");
        } else {
            response.setErrorMessage("Can't delete the book.");
        }

        return response;
    }

    private boolean validationParameters(Request request) {

        if (!(request instanceof DeleteBookRequest)) {
            return false;
        }

        DeleteBookRequest req = (DeleteBookRequest) request;
        if (req.getCommandName() != CommandName.DELETE_BOOK.name()) {
            return false;
        }

        if ((req.getAuthor() == null)
                || (req.getTitle() == null)
                || (req.getType() == null)) {
            return false;
        }

        return true;
    }
}
