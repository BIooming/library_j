package by.bsuir.lab01.controller.command.impl.book;

import by.bsuir.lab01.bean.Request;
import by.bsuir.lab01.bean.Response;
import by.bsuir.lab01.bean.book.NewBookRequest;
import by.bsuir.lab01.bean.book.NewBookResponse;
import by.bsuir.lab01.controller.command.Command;
import by.bsuir.lab01.controller.command.CommandException;
import by.bsuir.lab01.controller.command.CommandName;
import by.bsuir.lab01.entity.Book;
import by.bsuir.lab01.service.ModificationRepositoryService;
import by.bsuir.lab01.service.ServiceException;

public class AddNewBookCommand implements Command {

    @Override
    public Response execute(Request request) throws CommandException {

        if (!validationParameters(request)) {
            throw new CommandException("Validation Exception.");
        }

        NewBookRequest newBookRequest = (NewBookRequest) request;
        Book book = new Book(newBookRequest);

        boolean result = false;
        try {
            result = ModificationRepositoryService
                    .addNewBookService(book);
        } catch (ServiceException e) {
            throw new CommandException("Add book service exception", e);
        }

        NewBookResponse response = new NewBookResponse();
        if (result) {
            response.setResultMessage("Book added.");
        } else {
            response.setErrorMessage("Can't add the book.");
        }

        return response;
    }

    private boolean validationParameters(Request request) {

        if (!(request instanceof NewBookRequest)) {
            return false;
        }

        NewBookRequest req = (NewBookRequest) request;
        if (req.getCommandName() != CommandName.ADD_NEW_BOOK.name()) {
            return false;
        }

        if ((req.getTitle() == null)
                || (req.getTitle() == null)
                || (req.getType() == null)) {
            return false;
        }

        return true;
    }
}
