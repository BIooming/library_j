package by.bsuir.lab01.controller.command.impl.book;

import by.bsuir.lab01.bean.Request;
import by.bsuir.lab01.bean.Response;
import by.bsuir.lab01.bean.book.SearchBookRequest;
import by.bsuir.lab01.bean.book.SearchBookResponse;
import by.bsuir.lab01.controller.command.Command;
import by.bsuir.lab01.controller.command.CommandException;
import by.bsuir.lab01.entity.Book;
import by.bsuir.lab01.service.SearchBookService;
import by.bsuir.lab01.service.ServiceException;

import java.util.ArrayList;

/**
 * Created by blooming on 28.9.15.
 */
public class SearchBookCommand implements Command {

    @Override
    public Response execute(Request request) throws CommandException {
        int searchType = validateParameters(request);

        if (searchType == -1) {
            throw new CommandException("Validation exception");
        }

        SearchBookRequest searchBookRequest = (SearchBookRequest) request;

        ArrayList<Book> result = null;
        try {
            switch (searchType) {
                case 3:
                    result = SearchBookService.searchSpecificBook(
                            searchBookRequest.getTitle(),
                            searchBookRequest.getAuthor());
                    break;
                case 2:
                    result = SearchBookService.searchBookByAuthor(searchBookRequest.getAuthor());
                    break;
                case 1:
                    result = SearchBookService.searchBookByTitle(searchBookRequest.getTitle());
                    break;
                case 0:
                    result = SearchBookService.getAllBooks();
            }
        } catch (ServiceException ex) {
            throw new CommandException("Command exception: " + ex);
        }

        SearchBookResponse response = new SearchBookResponse();
        response.setBooks(result);
        return response;
    }

    /**
     * Validation of SearchUserRequest parameters
     *
     * @param request
     * @return int value:
     * -1 in case of validation failure;
     * 3 in case of both author and title params of request are set
     * 2 in case of only author parameter set
     * 1 in case of only title parameter set
     * 0 in case of no parameters set
     */
    private int validateParameters(Request request) {
        if (!(request instanceof SearchBookRequest)) {
            return -1;
        }

        SearchBookRequest req = (SearchBookRequest) request;

        if ((req.getTitle() != null) && (req.getAuthor() != null)) {
            return 3;
        }

        if (req.getAuthor() != null) {
            return 2;
        } else {
            if (req.getTitle() != null)
                return 1;
        }

        return 0;
    }
}
