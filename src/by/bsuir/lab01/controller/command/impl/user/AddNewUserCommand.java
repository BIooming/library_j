package by.bsuir.lab01.controller.command.impl.user;

import by.bsuir.lab01.bean.Request;
import by.bsuir.lab01.bean.Response;
import by.bsuir.lab01.bean.user.NewUserRequest;
import by.bsuir.lab01.bean.user.NewUserResponse;
import by.bsuir.lab01.controller.command.Command;
import by.bsuir.lab01.controller.command.CommandException;
import by.bsuir.lab01.controller.command.CommandName;
import by.bsuir.lab01.entity.User;
import by.bsuir.lab01.entity.UserCreationException;
import by.bsuir.lab01.service.ModificationRepositoryService;
import by.bsuir.lab01.service.ServiceException;

/**
 * Created by blooming on 25.9.15.
 */
public class AddNewUserCommand implements Command {

    @Override
    public Response execute(Request request) throws CommandException {

        if (!validationParameters(request)) {
            throw new CommandException("Request parameters are invalid");
        }

        NewUserRequest newUserRequest = (NewUserRequest) request;
        User user = null;

        try {
            user = new User(newUserRequest);
        } catch (UserCreationException e) {
            throw new CommandException("User creating exception", e);
        }

        boolean result = false;
        try {
            result = ModificationRepositoryService.addNewUserService(user);
        } catch (ServiceException e) {
            throw new CommandException("Add user service exception", e);
        }

        NewUserResponse response = new NewUserResponse();
        if (result) {
            response.setResultMessage("User added");
        } else {
            response.setErrorMessage("Can't add user");
        }
        return response;
    }

    public boolean validationParameters(Request request) {

        if (!(request instanceof NewUserRequest)) {
            return false;
        }

        NewUserRequest req = (NewUserRequest) request;
        if (req.getCommandName() != CommandName.ADD_NEW_USER.name()) {
            return false;
        }

        if ((req.getPassword().isEmpty())
                || (req.getUserName().isEmpty())
                || (req.getUserType() == null)) {
            return false;
        }

        return true;
    }
}
