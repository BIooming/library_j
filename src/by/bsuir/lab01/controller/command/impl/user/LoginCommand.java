package by.bsuir.lab01.controller.command.impl.user;

import by.bsuir.lab01.bean.Request;
import by.bsuir.lab01.bean.Response;
import by.bsuir.lab01.bean.user.LoginRequest;
import by.bsuir.lab01.bean.user.LoginResponse;
import by.bsuir.lab01.controller.command.Command;
import by.bsuir.lab01.controller.command.CommandException;
import by.bsuir.lab01.controller.command.CommandName;
import by.bsuir.lab01.entity.User;
import by.bsuir.lab01.entity.UserCreationException;
import by.bsuir.lab01.service.SearchUserService;
import by.bsuir.lab01.service.ServiceException;

public class LoginCommand implements Command {

    @Override
    public Response execute(Request request) throws CommandException {

        if (!validationParameters(request)) {
            throw new CommandException("Request parameters are invalid");
        }

        LoginRequest loginRequest = (LoginRequest) request;
        User user = null;
        User result = null;

        try {
            user = new User();
            user.setUserName(loginRequest.getUserName());
            user.setPassword(loginRequest.getPassword());

            result = SearchUserService.loginService(user);
        } catch (ServiceException e) {
            throw new CommandException("Login service exception: ", e);
        } catch (UserCreationException e) {
            throw new CommandException("User creation exception: ", e);
        }

        LoginResponse response = new LoginResponse();
        if (result != null) {
            response.setUser(result);
        } else {
            response.setErrorMessage("Can't login");
        }

        return response;
    }

    public boolean validationParameters(Request request) {

        if (!(request instanceof LoginRequest)) {
            return false;
        }

        LoginRequest req = (LoginRequest) request;
        if (req.getCommandName() != CommandName.LOGIN_USER.name()) {
            return false;
        }

        if ((req.getUserName() == null) || (req.getPassword() == null)) {
            return false;
        }

        return true;
    }
}
