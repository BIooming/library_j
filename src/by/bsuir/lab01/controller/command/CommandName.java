package by.bsuir.lab01.controller.command;

public enum CommandName {
    ADD_NEW_BOOK, SEARCH_BOOK, DELETE_BOOK, PREVIEW_BOOK,
    ADD_NEW_USER, LOGIN_USER, DELETE_USER,
    SEND_EMAIL
}
