package by.bsuir.lab01.controller.command;

import by.bsuir.lab01.controller.command.impl.book.AddNewBookCommand;
import by.bsuir.lab01.controller.command.impl.book.DeleteBookCommand;
import by.bsuir.lab01.controller.command.impl.book.SearchBookCommand;
import by.bsuir.lab01.controller.command.impl.user.AddNewUserCommand;
import by.bsuir.lab01.controller.command.impl.user.LoginCommand;

import java.util.HashMap;
import java.util.Map;

public final class CommandHelper {
    private Map<CommandName, Command> commands = new HashMap<CommandName, Command>();

    public CommandHelper() {
        commands.put(CommandName.ADD_NEW_BOOK, new AddNewBookCommand());
        commands.put(CommandName.LOGIN_USER, new LoginCommand());
        commands.put(CommandName.ADD_NEW_USER, new AddNewUserCommand());
        commands.put(CommandName.SEARCH_BOOK, new SearchBookCommand());
        commands.put(CommandName.DELETE_BOOK, new DeleteBookCommand());
    }

    public Command getCommand(String commandName) {
        CommandName command = CommandName.valueOf(commandName);

        return commands.get(command);
    }
}
