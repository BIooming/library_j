package by.bsuir.lab01.bean.user;

import by.bsuir.lab01.bean.Request;

/**
 * Created by blooming on 25.9.15.
 */
public class LoginRequest extends Request {
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
