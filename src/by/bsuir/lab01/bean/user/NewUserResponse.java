package by.bsuir.lab01.bean.user;

import by.bsuir.lab01.bean.Response;

/**
 * Created by blooming on 25.9.15.
 */
public class NewUserResponse extends Response {
    private String resultMessage;

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }
}
