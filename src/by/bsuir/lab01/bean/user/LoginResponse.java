package by.bsuir.lab01.bean.user;

import by.bsuir.lab01.bean.Response;
import by.bsuir.lab01.entity.User;

/**
 * Created by blooming on 25.9.15.
 */
public class LoginResponse extends Response {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
