package by.bsuir.lab01.bean.user;

import by.bsuir.lab01.bean.Request;
import by.bsuir.lab01.entity.UserType;

/**
 * Created by blooming on 25.9.15.
 */
public class NewUserRequest extends Request {
    private String userName;
    private String password;
    private String email;
    private UserType userType;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }
}
