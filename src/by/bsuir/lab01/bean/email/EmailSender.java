package by.bsuir.lab01.bean.email;

import by.bsuir.lab01.bean.book.NewBookRequest;
import by.bsuir.lab01.entity.User;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by blooming on 5.10.15.
 */
public class EmailSender {
    private static final String SMTPHOST_NAME = "smtp.gmail.com";
    private static final int SMTPHOST_PORT = 465;

    private static String smtpauthUser;

    public static void sendEmailToAdmin(NewBookRequest request, User user,
                                        String smtpathPassword) {
        try {
            Properties properties = new Properties();

            properties.put("mail.transport.protocol", "smtps");
            properties.put("mail.smtps.host", SMTPHOST_NAME);
            properties.put("mail.smtps.auth", "true");

            Session mailSession = Session.getDefaultInstance(properties);
            mailSession.setDebug(true);

            javax.mail.Transport transport = mailSession.getTransport();

            MimeMessage message1 = new MimeMessage(mailSession);
            message1.setSubject("Home library: new book proposition");
            message1.setContent("Hi, I want to propose current book for "
                    + "adding in library:\nTitle: " + request.getTitle()
                    + "\nAuthor: " + request.getAuthor()
                    + "\nType:" + request.getType(), "text/plain");
            message1.addRecipient(javax.mail.Message.RecipientType.TO, new
                    InternetAddress("eshekor@gmail.com"));

            smtpauthUser = user.getEmail();
            transport.connect(SMTPHOST_NAME, SMTPHOST_PORT, smtpauthUser,
                    smtpathPassword);

            transport.sendMessage(message1,
                    message1.getRecipients(javax.mail.Message.RecipientType.TO));
            transport.close();
        } catch (AddressException e) {
            System.out.println("Could not send email, address is incorrect");
        } catch (NoSuchProviderException e) {
            System.out.println("Provider not found");
        } catch (MessagingException e) {
            System.out.println("Error during sending message:" + e.getMessage());
        }
    }
}
