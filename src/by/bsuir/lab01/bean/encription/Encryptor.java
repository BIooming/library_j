package by.bsuir.lab01.bean.encription;

import java.security.MessageDigest;

/**
 * Created by blooming on 25.9.15.
 */
public class Encryptor {

    public static String encrypt(String string) throws EncryptorException {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(string.getBytes());

            StringBuffer stringBuffer = new StringBuffer();

            for (byte byteElement : md.digest()) {
                stringBuffer.append(Integer.toString((byteElement & 0xff) +
                        0x100, 16).substring(1));
            }
            return stringBuffer.toString();
        } catch (Exception e) {
            throw new EncryptorException("Cannot encrypt password", e);
        }
    }
}
