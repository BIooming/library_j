package by.bsuir.lab01.bean.encription;

/**
 * Created by blooming on 25.9.15.
 */
public class EncryptorException extends Exception {
    private static final long serialVersionUID = 1L;

    public EncryptorException(String message) {
        super(message);
    }

    public EncryptorException(String message, Exception ex) {
        super(message, ex);
    }
}
