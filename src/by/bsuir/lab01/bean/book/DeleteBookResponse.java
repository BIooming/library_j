package by.bsuir.lab01.bean.book;

import by.bsuir.lab01.bean.Response;

/**
 * Created by blooming on 5.10.15.
 */
public class DeleteBookResponse extends Response {
    private String resultMessage;

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

}
