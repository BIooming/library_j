package by.bsuir.lab01.bean.book;

import by.bsuir.lab01.bean.Request;
import by.bsuir.lab01.entity.BookType;

/**
 * Created by blooming on 5.10.15.
 */
public class DeleteBookRequest extends Request {
    private String title;
    private String author;
    private BookType type;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public BookType getType() {
        return type;
    }

    public void setType(BookType type) {
        this.type = type;
    }

}
