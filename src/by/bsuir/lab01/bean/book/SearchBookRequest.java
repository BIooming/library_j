package by.bsuir.lab01.bean.book;

import by.bsuir.lab01.bean.Request;

/**
 * Created by blooming on 28.9.15.
 */
public class SearchBookRequest extends Request {
    public String title;
    public String author;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
