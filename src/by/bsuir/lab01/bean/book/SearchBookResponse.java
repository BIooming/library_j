package by.bsuir.lab01.bean.book;

import by.bsuir.lab01.bean.Response;
import by.bsuir.lab01.entity.Book;

import java.util.ArrayList;

/**
 * Created by blooming on 28.9.15.
 */
public class SearchBookResponse extends Response {
    private ArrayList<Book> books;

    public ArrayList<Book> getBooks() {
        return books;
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
    }
}
