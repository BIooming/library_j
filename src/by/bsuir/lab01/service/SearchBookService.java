package by.bsuir.lab01.service;


import by.bsuir.lab01.dao.DaoException;
import by.bsuir.lab01.dao.DaoFactory;
import by.bsuir.lab01.dao.SearchDao;
import by.bsuir.lab01.entity.Book;

import java.util.ArrayList;

public class SearchBookService {
    private static DaoFactory daoFactory = DaoFactory.getDaoFactory();
    private static SearchDao searchDao = daoFactory.getSearchDao();

    public static ArrayList<Book> searchBookByAuthor(String author)
            throws ServiceException {
        try {
            return searchDao.searchBookByAuthor(author);
        } catch (DaoException ex) {
            throw new ServiceException("Service exception: " + ex);
        }
    }

    public static ArrayList<Book> searchBookByTitle(String title)
            throws ServiceException {
        try {
            return searchDao.searchBookByTitle(title);
        } catch (DaoException ex) {
            throw new ServiceException("Service exception: " + ex);
        }
    }

    public static ArrayList<Book> searchSpecificBook(String title, String author)
            throws ServiceException {
        try {
            return searchDao.searchSpecificBook(title, author);
        } catch (DaoException ex) {
            throw new ServiceException("Service exception: " + ex);
        }
    }

    public static ArrayList<Book> getAllBooks() throws ServiceException {
        try {
            return searchDao.getAllBooks();
        } catch (DaoException ex) {
            throw new ServiceException("Service exception: " + ex);
        }
    }

}
