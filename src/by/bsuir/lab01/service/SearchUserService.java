package by.bsuir.lab01.service;

import by.bsuir.lab01.dao.DaoException;
import by.bsuir.lab01.dao.DaoFactory;
import by.bsuir.lab01.dao.SearchDao;
import by.bsuir.lab01.entity.User;

/**
 * Created by blooming on 25.9.15.
 */
public class SearchUserService {

    public static User loginService(User user) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        SearchDao searchDao = daoFactory.getSearchDao();

        try {
            return searchDao.login(user);
        } catch (DaoException e) {
            throw new ServiceException("Service exception: " + e.getMessage());
        }
    }
}
