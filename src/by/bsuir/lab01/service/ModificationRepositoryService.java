package by.bsuir.lab01.service;

import by.bsuir.lab01.dao.DaoException;
import by.bsuir.lab01.dao.DaoFactory;
import by.bsuir.lab01.dao.ModificationDao;
import by.bsuir.lab01.entity.Book;
import by.bsuir.lab01.entity.User;

public final class ModificationRepositoryService {
    private ModificationRepositoryService() {
    }

    public static boolean addNewBookService(Book book) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        ModificationDao modificationDao = daoFactory.getModificationDao();

        try {
            return modificationDao.addNewBook(book);
        } catch (DaoException e) {
            throw new ServiceException("Data access exception", e);
        }
    }

    public static boolean deleteBookService(Book book) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        ModificationDao modificationDao = daoFactory.getModificationDao();

        try {
            return modificationDao.deleteBook(book);
        } catch (DaoException e) {
            throw new ServiceException("Data access exception", e);
        }
    }

    public static boolean addNewUserService(User user) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        ModificationDao modificationDao = daoFactory.getModificationDao();

        try {
            return modificationDao.addNewUser(user);
        } catch (DaoException e) {
            throw new ServiceException("Data access exception", e);
        }
    }
}
