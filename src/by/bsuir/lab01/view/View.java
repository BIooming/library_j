package by.bsuir.lab01.view;

import by.bsuir.lab01.view.factory.ViewFactory;
import by.bsuir.lab01.view.factory.Viewable;

public class View {
    private static ViewFactory viewFactory = ViewFactory.getViewFactory();
    private static Viewable viewable = viewFactory.getConsoleView();

    public void menu() {
        viewable.viewMenu();
    }
}
