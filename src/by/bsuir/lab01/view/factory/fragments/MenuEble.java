package by.bsuir.lab01.view.factory.fragments;

import by.bsuir.lab01.bean.Response;
import by.bsuir.lab01.bean.book.*;
import by.bsuir.lab01.bean.email.EmailSender;
import by.bsuir.lab01.bean.user.LoginRequest;
import by.bsuir.lab01.bean.user.LoginResponse;
import by.bsuir.lab01.bean.user.NewUserRequest;
import by.bsuir.lab01.bean.user.NewUserResponse;
import by.bsuir.lab01.controller.BookController;
import by.bsuir.lab01.controller.UserController;
import by.bsuir.lab01.controller.command.CommandName;
import by.bsuir.lab01.entity.Book;
import by.bsuir.lab01.entity.BookType;
import by.bsuir.lab01.entity.User;
import by.bsuir.lab01.entity.UserType;
import by.bsuir.lab01.view.factory.Viewable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by blooming on 1.10.15.
 */
public class MenuEble implements Viewable {
    private final static MenuEble instance = new MenuEble();

    private static BufferedReader bufferedReader = new BufferedReader(new
            InputStreamReader(System.in));
    private static UserController userController = new UserController();
    private static BookController bookController = new BookController();
    private static User currentUser;

    private String input = null;

    private MenuEble() {
    }

    public static MenuEble getInstance() {
        return instance;
    }

    @Override
    public void viewMenu() {
        int selection = 1;

        do {
            System.out.println("\n+++ Welcome to the Librarium login Menu +++");
            System.out.println("Please make a selection:\n" +
                    "[1] Login\n" +
                    "[2] Register\n" +
                    "[0] Exit");
            try {
                input = bufferedReader.readLine();
                selection = Integer.parseInt(input);

                switch (selection) {
                    case 1:
                        currentUser = viewLoginMenu();

                        if (UserType.ADMIN.equals(currentUser.getUserType())) {
                            viewAdminMenu();
                        } else
                            viewGuestMenu();
                        break;
                    case 2:
                        viewRegisterMenu(1);
                        viewMenu();
                        break;
                    case 0:
                        break;
                    default:
                        System.out.println("Invalid input\n");
                        break;
                }
            } catch (NumberFormatException e) {
                System.out.println("Entered value is not a number. Please try" +
                        " again.\n");
            } catch (IOException e) {
                System.out.println("IOException\n");
            } catch (NullPointerException e) {
                System.out.println("User not found or password is incorrect\n");
            }
        } while (selection != 0);
    }

    @Override
    public void viewRegisterMenu(int accessLevel) {
        NewUserRequest request = new NewUserRequest();

        try {
            System.out.println("\n+++ New user creation menu +++");
            System.out.println("+ Please enter new user data +");

            System.out.println("User name: ");
            request.setUserName(bufferedReader.readLine());

            System.out.println("Email: ");
            request.setEmail(bufferedReader.readLine());

            System.out.println("Password: ");
            request.setPassword(bufferedReader.readLine());

            request.setUserType(setUserTypeView(accessLevel));

            request.setCommandName(CommandName.ADD_NEW_USER.name());

            Response response = userController.executeRequest(request);

            if (response.getErrorMessage() != null) {
                System.out.println(response.getErrorMessage());
            } else {
                NewUserResponse newUserResponse = (NewUserResponse) response;
                System.out.println(newUserResponse.getResultMessage());
            }
        } catch (IOException e) {
            System.out.println("Input/Output error");
        }
    }

    private UserType setUserTypeView(int accessLevel) {

        if (accessLevel == 2) {
            do {
                System.out.println("Select User Type:\n[1] - Admin\n[2] - " +
                        "Guest");
                try {
                    input = bufferedReader.readLine();

                    switch (Integer.parseInt(input)) {
                        case 1:
                            return UserType.ADMIN;
                        case 2:
                            return UserType.GUEST;
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Value is not a number");
                } catch (IOException e) {
                    System.out.println("Input/Output exception");
                }
            } while (true);
        } else {
            return UserType.GUEST;
        }
    }

    @Override
    public User viewLoginMenu() {
        LoginRequest request = new LoginRequest();

        try {
            System.out.println("\n+++ Login Menu +++");

            System.out.println("Enter login:");
            request.setUserName(bufferedReader.readLine());

            System.out.println("Enter password");
            request.setPassword(bufferedReader.readLine());

            request.setCommandName(CommandName.LOGIN_USER.name());

            Response response = userController.executeRequest(request);

            if (response.getErrorMessage() != null) {
                System.out.println(response.getErrorMessage());
                return null;
            } else {
                LoginResponse loginResponse = (LoginResponse) response;
                currentUser = loginResponse.getUser();

                System.out.println("Logged User:\n" + loginResponse.getUser()
                        .getUserName()
                        + "\n" + loginResponse.getUser().getUserType() + "\n");
                return loginResponse.getUser();
            }
        } catch (IOException e) {
            System.out.println("Input/Output exception");
            return null;
        }
    }

    @Override
    public void viewGuestMenu() {
        int selection = -1;

        do {
            System.out.println("\n+++++++ Guest Menu +++++++");
            System.out.println("Please make a selection:\n" +
                    "[1] Review book catalog\n" +
                    "[2] Search book\n" +
                    "[3] Propose book to add\n" +
                    "[0] Logout\n");

            try {
                input = bufferedReader.readLine();
                selection = Integer.parseInt(input);

                switch (selection) {
                    case 1:
                        viewCatalogMenu();
                        break;
                    case 2:
                        viewSearchMenu();
                        break;
                    case 3:
                        addBook(1);
                        break;
                }
            } catch (NumberFormatException e) {
                System.out.println("Entered value is not a number please try " +
                        "again");
            } catch (IOException e) {
                System.out.println("Input/Output exception");
            }
        } while (selection != 0);
    }

    @Override
    public void viewAdminMenu() {
        int selection = -1;

        do {
            System.out.println("\n+++++++ Administrator Menu +++++++");
            System.out.println("Please make a selection:\n" +
                    "[1] Review book catalog\n" +
                    "[2] Search book\n" +
                    "[3] Edit catalog\n" +
                    "[4] Add user\n" +
                    "[0] Logout\n");

            try {
                input = bufferedReader.readLine();
                selection = Integer.parseInt(input);

                switch (selection) {
                    case 1:
                        viewCatalogMenu();
                        break;
                    case 2:
                        viewSearchMenu();
                        break;
                    case 3:
                        viewCatalogEditMenu();
                        break;
                    case 4:
                        viewRegisterMenu(2);
                        break;
                }
            } catch (IOException e) {
                System.out.println("Input/Output error");
            } catch (NumberFormatException e) {
                System.out.println("Entered value is not a number please try " +
                        "again");
            }
        } while (selection != 0);
    }

    @Override
    public void viewSearchMenu() {
        SearchBookRequest request;
        int selection = -1;

        do {
            try {

                System.out.println("\n+++ Search Menu +++");
                System.out.println("Please make a selection:\n" +
                        "[1] Search by Author\n" +
                        "[2] Search by Title\n" +
                        "[3] Search by Author&Title\n" +
                        "[0] Back\n");

                input = bufferedReader.readLine();
                selection = Integer.parseInt(input);

                if (selection != 0) {
                    request = viewSearchBookReqEdit(selection);
                    Response response = bookController.executeRequest(request);

                    if (response.getErrorMessage() != null) {
                        System.out.println("Book searching error: " + response
                                .getErrorMessage());
                    } else {
                        viewFoundBooks(response);
                    }
                }
            } catch (IOException e) {
                System.out.println("Input/Output error");
            } catch (NumberFormatException e) {
                System.out.println("Entered value is not a number please try " +
                        "again");
            }
        } while (selection != 0);
    }

    private SearchBookRequest viewSearchBookReqEdit(int selection) {
        SearchBookRequest request = new SearchBookRequest();

        try {
            switch (selection) {
                case 1:
                    System.out.println("\nEnter author:");
                    request.setAuthor(bufferedReader.readLine());
                    request.setTitle(null);
                    break;
                case 2:
                    System.out.println("\nEnter title:");
                    request.setTitle(bufferedReader.readLine());
                    request.setAuthor(null);
                    break;
                case 3:
                    System.out.println("\nEnter author:");
                    request.setAuthor(bufferedReader.readLine());
                    System.out.println("\nEnter title:");
                    request.setTitle(bufferedReader.readLine());
                    break;
            }
            request.setCommandName(CommandName.SEARCH_BOOK.name());

            return request;
        } catch (IOException e) {
            System.out.println("Input/output exception");
            return null;
        }
    }

    private void viewFoundBooks(Response response) {
        SearchBookResponse searchBookResponse = (SearchBookResponse)
                response;

        if (!searchBookResponse.getBooks().isEmpty()) {
            System.out.println("\nFound book(s):\n");

            for (Book book : searchBookResponse.getBooks()) {
                System.out.println("+Author: " + book.getAuthor());
                System.out.println("+Title: " + book.getTitle());
                System.out.println("+Type: " + book.getType().name());
                System.out.println("--------------------------------");
            }
        } else {
            System.out.println("No books found");
        }
    }

    @Override
    public void viewCatalogMenu() {
        ArrayList<ArrayList<Book>> catalog = new ArrayList<>();
        SearchBookRequest request = new SearchBookRequest();

        request.setCommandName(CommandName.SEARCH_BOOK.name());
        Response response = bookController.executeRequest(request);

        if (response.getErrorMessage() != null) {
            System.out.println("Book searching error: " + response
                    .getErrorMessage());
        } else {
            catalog = fillBooksCatalog(response);

            if (!catalog.isEmpty()) {
                System.out.println("\nFound book(s):");
                displayCatalog(catalog);
            } else {
                System.out.println("No books found");
            }
        }
    }

    private ArrayList<ArrayList<Book>> fillBooksCatalog(Response response) {
        int page = 0;
        ArrayList<ArrayList<Book>> catalog = new ArrayList<>();

        SearchBookResponse searchBookResponse = (SearchBookResponse)
                response;

        catalog.add(new ArrayList<Book>());
        for (Book book : searchBookResponse.getBooks()) {
            if (catalog.get(page).size() <= 5) {
                catalog.get(page).add(book);
            } else {
                catalog.add(new ArrayList<Book>());
                page++;
                catalog.get(page).add(book);
            }
        }

        return catalog;
    }

    private void displayCatalog(ArrayList<ArrayList<Book>> catalog) {
        int selection = -1;
        int page = 0;

        do {
            try {
                for (Book book : catalog.get(page)) {
                    System.out.println("+Title: " + book.getTitle());
                    System.out.println("+Author: " + book.getAuthor());
                    System.out.println("+Type: " + book.getType()
                            .name());
                    System.out.println
                            ("--------------------------------");
                }
                if (page < catalog.size() - 1) {
                    System.out.println("\n[1] Next page");
                }
                if (page != 0) {
                    System.out.println("[2] Previous page");
                }
                System.out.println("[0] Back");

                input = bufferedReader.readLine();
                selection = Integer.parseInt(input);

                switch (selection) {
                    case 1:
                        page++;
                        break;
                    case 2:
                        page--;
                }
            } catch (IOException e) {
                System.out.println("Input/Output error");
            } catch (NumberFormatException e) {
                System.out.println("Entered value is not a number please try " +
                        "again");
            }
        } while (selection != 0);
    }

    @Override
    public void viewCatalogEditMenu() {
        int selection = -1;

        try {
            do {
                System.out.println("\n+++ Catalog Edit Menu +++");
                System.out.println("Please make a selection:\n" +
                        "[1] Add book\n" +
                        "[2] Delete book\n" +
                        "[0] Back\n");

                input = bufferedReader.readLine();
                selection = Integer.parseInt(input);

                switch (selection) {
                    case 1:
                        addBook(2);
                        break;
                    case 2:
                        deleteBook();
                }
            } while (selection != 0);
        } catch (IOException e) {
            System.out.println("Input/Output error");
        } catch (NumberFormatException e) {
            System.out.println("Entered value is not a number please try " +
                    "again");
        }
    }

    private void deleteBook() {
        DeleteBookRequest request = new DeleteBookRequest();

        try {
            System.out.println("+++ Book delete menu +++");
            System.out.println("+ Please enter data of book you want to " +
                    "delete: +");

            System.out.println("Book title: ");
            request.setTitle(bufferedReader.readLine());

            System.out.println("Book author: ");
            request.setAuthor(bufferedReader.readLine());

            request.setType(viewBookTypeSelection());

            request.setCommandName(CommandName.DELETE_BOOK.name());

            Response response = bookController.executeRequest(request);
            if (response.getErrorMessage() != null) {
                System.out.println(response.getErrorMessage());
            } else {
                DeleteBookResponse deleteBookResponse = (DeleteBookResponse)
                        response;
                System.out.println(deleteBookResponse.getResultMessage());
            }
        } catch (IOException e) {
            System.out.println("Input/Output error");
        }
    }

    private int addBook(int acessLevel) {
        int selection = -1;

        NewBookRequest request = new NewBookRequest();
        try {
            System.out.println("+++ Book add menu +++");
            System.out.println("+ Please enter data of book you want to add: " +
                    "+");

            System.out.println("Book title: ");
            request.setTitle(bufferedReader.readLine());
            System.out.println("Book author: ");
            request.setAuthor(bufferedReader.readLine());

            request.setType(viewBookTypeSelection());

            if (acessLevel == 2) {
                request.setCommandName(CommandName.ADD_NEW_BOOK.name());

                Response response = bookController.executeRequest(request);
                if (response.getErrorMessage() != null) {
                    System.out.println(response.getErrorMessage());
                    return -1;
                } else {
                    DeleteBookResponse newUserResponse = (DeleteBookResponse)
                            response;
                    System.out.println(newUserResponse.getResultMessage());
                    return 0;
                }
            } else {
                System.out.println("Please enter your email password (it " +
                        "wouldn't be saved)");
                String password = bufferedReader.readLine();
                EmailSender.sendEmailToAdmin(request, currentUser, password);
            }
        } catch (IOException e) {
            System.out.println("Input/Output error");
        }

        return selection;
    }

    private BookType viewBookTypeSelection() {
        int selection = -1;

        try {
            do {
                System.out.println("Book type:\n[1] - Ebook\n[2] - Paper book");
                try {
                    input = bufferedReader.readLine();
                    switch (Integer.parseInt(input)) {
                        case 1:
                            return BookType.E_BOOK;
                        case 2:
                            return BookType.PAPER_BOOK;
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Value is not a number");
                }
            } while (selection != 0);
        } catch (IOException e) {
            System.out.println("Input/Output error");
        }

        return null;
    }
}