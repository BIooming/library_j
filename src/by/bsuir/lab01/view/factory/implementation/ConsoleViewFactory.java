package by.bsuir.lab01.view.factory.implementation;

import by.bsuir.lab01.view.factory.ViewFactory;
import by.bsuir.lab01.view.factory.Viewable;
import by.bsuir.lab01.view.factory.fragments.MenuEble;

/**
 * Created by blooming on 1.10.15.
 */
public class ConsoleViewFactory extends ViewFactory {
    private final static ConsoleViewFactory instance = new ConsoleViewFactory();

    private ConsoleViewFactory() {
    }

    public final static ConsoleViewFactory getInstance() {
        return instance;
    }

    @Override
    public Viewable getConsoleView() {
        return MenuEble.getInstance();
    }
}
