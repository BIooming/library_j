package by.bsuir.lab01.view.factory;

import by.bsuir.lab01.start.PropertiesController;
import by.bsuir.lab01.view.factory.implementation.ConsoleViewFactory;

/**
 * Created by blooming on 1.10.15.
 */
public abstract class ViewFactory {
    private static final String VIEW_TYPE = PropertiesController.getProperty("view_type");//must read it from property file

    public static ViewFactory getViewFactory() {
        switch (VIEW_TYPE) {
            case "console":
                return ConsoleViewFactory.getInstance();
        }
        return null;
    }

    public abstract Viewable getConsoleView();
}
