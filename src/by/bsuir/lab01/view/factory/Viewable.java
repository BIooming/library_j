package by.bsuir.lab01.view.factory;

import by.bsuir.lab01.entity.User;

/**
 * Created by blooming on 1.10.15.
 */
public interface Viewable {

    void viewMenu();

    void viewRegisterMenu(int accessLevel);

    void viewCatalogEditMenu();

    void viewCatalogMenu();

    void viewSearchMenu();

    User viewLoginMenu();

    void viewGuestMenu();

    void viewAdminMenu();
}
