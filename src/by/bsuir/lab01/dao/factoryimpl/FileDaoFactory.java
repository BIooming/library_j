package by.bsuir.lab01.dao.factoryimpl;

import by.bsuir.lab01.dao.DaoFactory;
import by.bsuir.lab01.dao.ModificationDao;
import by.bsuir.lab01.dao.SearchDao;
import by.bsuir.lab01.dao.file.FileModificationDao;
import by.bsuir.lab01.dao.file.FileSearchDao;

public final class FileDaoFactory extends DaoFactory {
    private final static FileDaoFactory instance = new FileDaoFactory();

    private FileDaoFactory() {
    }

    public final static FileDaoFactory getInstance() {
        return instance;
    }

    @Override
    public SearchDao getSearchDao() {
        return FileSearchDao.getInstance();
    }

    @Override
    public ModificationDao getModificationDao() {
        return FileModificationDao.getInstance();
    }

}
