package by.bsuir.lab01.dao;

import by.bsuir.lab01.entity.Book;
import by.bsuir.lab01.entity.User;

public interface ModificationDao {

    boolean addNewBook(Book book) throws DaoException;

    boolean deleteBook(Book book) throws DaoException;

    boolean addNewUser(User user) throws DaoException;
}
