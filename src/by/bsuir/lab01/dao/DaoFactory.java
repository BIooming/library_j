package by.bsuir.lab01.dao;

import by.bsuir.lab01.dao.factoryimpl.FileDaoFactory;
import by.bsuir.lab01.start.PropertiesController;

public abstract class DaoFactory {
    private static final String DAO_TYPE =
            PropertiesController.getProperty("dao_type");

    public static DaoFactory getDaoFactory() {
        switch (DAO_TYPE) {
            case "file":
                return FileDaoFactory.getInstance();
        }
        return null;
    }


    public abstract SearchDao getSearchDao();

    public abstract ModificationDao getModificationDao();
}
