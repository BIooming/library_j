package by.bsuir.lab01.dao.file;

import by.bsuir.lab01.dao.DaoException;
import by.bsuir.lab01.dao.ModificationDao;
import by.bsuir.lab01.entity.Book;
import by.bsuir.lab01.entity.User;
import by.bsuir.lab01.start.PropertiesController;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public final class FileModificationDao implements ModificationDao {
    private static final String BOOKS_TXT =
            PropertiesController.getProperty("books_txt");
    private static final String USERS_TXT =
            PropertiesController.getProperty("users_txt");

    private static FileModificationDao instance;

    private File bookFile = null;
    private File userFile = null;


    private FileModificationDao() {
        bookFile = new File(BOOKS_TXT);
        userFile = new File(USERS_TXT);
    }

    public static FileModificationDao getInstance() {
        if (instance == null) {
            instance = new FileModificationDao();
            return instance;
        } else {
            return instance;
        }
    }

    public boolean addNewBook(Book book) throws DaoException {
        BufferedWriter bufferedWriter = null;

        try {
            if (!bookFile.exists()) {
                bookFile.createNewFile();
            }

            FileWriter fileWriter = new FileWriter(bookFile, true);
            bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.append("TITLE:" + book.getTitle() + "\nAUTHOR:" +
                    book.getAuthor() + "\nTYPE:" + book.getType().name() +
                    "\n");
        } catch (IOException ex) {
            throw new DaoException("Dao ExceptionMessage", ex);
        } finally {
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ex) {
                    System.out.println("Error in closing file-writer");
                }
            }
        }

        return true;
    }

    public boolean addNewUser(User user) throws DaoException {
        BufferedWriter bufferedWriter = null;

        try {
            if (!userFile.exists()) {
                userFile.createNewFile();
            }

            FileWriter fileWriter = new FileWriter(userFile, true);
            bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.append("User:" + user.getUserName()
                    + "\nPassword:" + user.getPassword()
                    + "\nEmail:" + user.getEmail()
                    + "\nType:" + user.getUserType() + "\n");
        } catch (IOException ex) {
            throw new DaoException("Dao ExceptionMessage", ex);
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            } catch (Exception ex) {
                throw new DaoException("Dao ExceptionMessage", ex);
            }
        }

        return true;
    }

    public boolean deleteBook(Book deletingBook) throws DaoException {
        BufferedWriter bufferedWriter = null;
        ArrayList<Book> books = FileSearchDao.getInstance().getAllBooks();

        try {
            if (!bookFile.exists()) {
                bookFile.createNewFile();
            }

            FileWriter fileWriter = new FileWriter(bookFile, false);
            bufferedWriter = new BufferedWriter(fileWriter);

            for (Book book : books) {
                if (!book.equals(deletingBook)) {
                    bufferedWriter.write("TITLE:" + book.getTitle()
                            + "\nAUTHOR:" + book.getAuthor()
                            + "\nTYPE:" + book.getType().name() + "\n");
                }
            }
        } catch (IOException ex) {
            throw new DaoException("Dao ExceptionMessage", ex);
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            } catch (Exception ex) {
                throw new DaoException("Dao ExceptionMessage", ex);
            }
        }

        return true;
    }
}
