package by.bsuir.lab01.dao.file;

import by.bsuir.lab01.dao.DaoException;
import by.bsuir.lab01.dao.SearchDao;
import by.bsuir.lab01.entity.Book;
import by.bsuir.lab01.entity.BookType;
import by.bsuir.lab01.entity.User;
import by.bsuir.lab01.entity.UserType;
import by.bsuir.lab01.start.PropertiesController;

import java.io.*;
import java.util.ArrayList;

public final class FileSearchDao implements SearchDao {
    private final static FileSearchDao instance = new FileSearchDao();

    private static final String BOOKS_TXT =
            PropertiesController.getProperty("books_txt");
    private static final String USERS_TXT =
            PropertiesController.getProperty("users_txt");

    private File bookFile = null;
    private File userFile = null;

    private FileSearchDao() {
    }

    public static FileSearchDao getInstance() {
        return instance;
    }

    @Override
    public ArrayList<Book> searchBookByAuthor(String author) throws DaoException {
        ArrayList<Book> bookList = new ArrayList<>();
        Book bufferedBook = null;

        try {
            bookFile = new File(BOOKS_TXT);
            BufferedReader reader = new BufferedReader(new FileReader(bookFile));
            String text = null;

            while ((text = reader.readLine()) != null) {
                String[] resultText = text.split(":");
                switch (resultText[0]) {
                    case "TITLE":
                        bufferedBook = new Book();
                        bufferedBook.setTitle(resultText[1]);
                        break;
                    case "AUTHOR":
                        if (bufferedBook != null)
                            bufferedBook.setAuthor(resultText[1]);
                        break;
                    case "TYPE":
                        if (bufferedBook != null) {
                            if (resultText[1].matches(BookType.E_BOOK.name()))
                                bufferedBook.setType(BookType.E_BOOK);
                            else
                                bufferedBook.setType(BookType.PAPER_BOOK);
                            if (bufferedBook.getAuthor().equals(author)) {
                                bookList.add(bufferedBook);
                            }
                        }
                }
            }
        } catch (Exception ex) {
            throw new DaoException("Dao exception message: " + ex);
        }

        return bookList;
    }

    @Override
    public ArrayList<Book> searchBookByTitle(String title) throws DaoException {
        ArrayList<Book> bookList = new ArrayList<>();
        Book bufferedBook = null;

        try {
            bookFile = new File(BOOKS_TXT);
            BufferedReader reader = new BufferedReader(new FileReader(bookFile));
            String text = null;

            while ((text = reader.readLine()) != null) {
                String[] resultText = text.split(":");
                switch (resultText[0]) {
                    case "TITLE":
                        bufferedBook = new Book();
                        bufferedBook.setTitle(resultText[1]);
                        break;
                    case "AUTHOR":
                        if (bufferedBook != null)
                            bufferedBook.setAuthor(resultText[1]);
                        break;
                    case "TYPE":
                        if (bufferedBook != null) {
                            if (resultText[1].matches(BookType.E_BOOK.name()))
                                bufferedBook.setType(BookType.E_BOOK);
                            else
                                bufferedBook.setType(BookType.PAPER_BOOK);
                            if (bufferedBook.getTitle().equals(title)) {
                                bookList.add(bufferedBook);
                            }
                        }
                }
            }
        } catch (FileNotFoundException e) {
            throw new DaoException("File not found");
        } catch (IOException e) {
            throw new DaoException("Input/Output error");
        }

        return bookList;
    }

    @Override
    public ArrayList<Book> searchSpecificBook(String author, String title) throws DaoException {
        ArrayList<Book> bookList = new ArrayList<>();
        Book bufferedBook = null;

        try {
            bookFile = new File(BOOKS_TXT);
            BufferedReader reader = new BufferedReader(new FileReader(bookFile));
            String text = null;

            while ((text = reader.readLine()) != null) {
                String[] resultText = text.split(":");
                switch (resultText[0]) {
                    case "TITLE":
                        bufferedBook = new Book();
                        bufferedBook.setTitle(resultText[1]);
                        break;
                    case "AUTHOR":
                        if (bufferedBook != null)
                            bufferedBook.setAuthor(resultText[1]);
                        break;
                    case "TYPE":
                        if (bufferedBook != null) {
                            if (resultText[1].matches(BookType.E_BOOK.name()))
                                bufferedBook.setType(BookType.E_BOOK);
                            else
                                bufferedBook.setType(BookType.PAPER_BOOK);
                            if (bufferedBook.getAuthor().equals(author) && bufferedBook.getTitle().equals(title)) {
                                bookList.add(bufferedBook);
                            }
                        }
                }
            }
        } catch (Exception ex) {
            throw new DaoException("Dao exception message: " + ex);
        }

        return bookList;
    }

    @Override
    public ArrayList<Book> getAllBooks() throws DaoException {
        ArrayList<Book> bookList = new ArrayList<>();
        Book bufferedBook = null;

        try {
            bookFile = new File(BOOKS_TXT);
            BufferedReader reader = new BufferedReader(new FileReader(bookFile));
            String text = null;

            while ((text = reader.readLine()) != null) {
                String[] resultText = text.split(":");
                switch (resultText[0]) {
                    case "TITLE":
                        bufferedBook = new Book();
                        bufferedBook.setTitle(resultText[1]);
                        break;
                    case "AUTHOR":
                        if (bufferedBook != null)
                            bufferedBook.setAuthor(resultText[1]);
                        break;
                    case "TYPE":
                        if (bufferedBook != null) {
                            if (resultText[1].matches(BookType.E_BOOK.name()))
                                bufferedBook.setType(BookType.E_BOOK);
                            else
                                bufferedBook.setType(BookType.PAPER_BOOK);
                            bookList.add(bufferedBook);
                        }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bookList;
    }

    public User login(User user) throws DaoException {
        User bufferUser = null;

        try {
            userFile = new File(USERS_TXT);
            BufferedReader reader = new BufferedReader(new FileReader(userFile));
            String text = null;

            while ((text = reader.readLine()) != null) {
                String[] resultText = text.split(":");
                switch (resultText[0]) {
                    case "User":
                        bufferUser = new User();
                        bufferUser.setUserName(resultText[1]);
                        break;
                    case "Password":
                        if (bufferUser != null) {
                            bufferUser.setHashedPassword(resultText[1]);
                        }
                        break;
                    case "Email":
                        if (bufferUser != null) {
                            bufferUser.setEmail(resultText[1]);
                        }
                        break;
                    case "Type":
                        if (bufferUser != null) {
                            if (resultText[1].matches("ADMIN")) {
                                bufferUser.setUserType(UserType.ADMIN);
                            } else bufferUser.setUserType(UserType.GUEST);
                            if (user.getUserName().equals(bufferUser.getUserName())
                                    && user.getPassword().equals(bufferUser.getPassword())) {
                                return bufferUser;
                            }
                        }
                }
            }
        } catch (Exception ex) {
            throw new DaoException("Dao exception message: " + ex);
        }
        return null;
    }
}
