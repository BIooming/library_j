package by.bsuir.lab01.dao;

import by.bsuir.lab01.entity.Book;
import by.bsuir.lab01.entity.User;

import java.util.ArrayList;

public interface SearchDao {

    ArrayList<Book> searchBookByAuthor(String author) throws DaoException;

    ArrayList<Book> searchBookByTitle(String title) throws DaoException;

    ArrayList<Book> searchSpecificBook(String author, String title) throws DaoException;

    ArrayList<Book> getAllBooks() throws DaoException;

    User login(User user) throws DaoException;
}
